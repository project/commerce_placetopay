<?php

namespace Drupal\commerce_placetopay\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides route responses for the Example module.
 */
class CheckoutErrorController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function errorPage(OrderInterface $order, $response_status) {
    return [
      '#theme' => 'commerce_checkout_error',
      '#order_entity' => $order,
      '#response_status' => $response_status,
    ];
  }

}
