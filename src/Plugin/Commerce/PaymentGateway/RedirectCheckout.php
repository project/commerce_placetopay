<?php

namespace Drupal\commerce_placetopay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_placetopay\CheckoutAuth;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides the placetopay offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_placetopay_redirect_checkout",
 *   label = @Translation("PlaceToPay Checkout (Redirect to PlaceToPay Checkout)"),
 *   display_label = @Translation("PlaceToPay Checkout (Pay With Card)"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_placetopay\PluginForm\RedirectCheckoutPaymentForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class RedirectCheckout extends OffsitePaymentGatewayBase implements RedirectCheckoutInterface {

  /**
   * The checkout auth services.
   *
   * @var \Drupal\commerce_placetopay\CheckoutAuth
   */
  protected $checkoutauth;

  /**
   * The Payment.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $requestId;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->checkout_auth = new CheckoutAuth($configuration);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'login_key' => '',
      'secret_key' => '',
      'test_endpoint_url' => '',
      'live_endpoint_url' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['login_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Login Key'),
      '#description' => $this->t('Unique site identifier given by PlaceToPay.'),
      '#default_value' => $this->configuration['login_key'],
      '#required' => TRUE,
    ];

    $form['secret_key'] = [
      '#type' => 'password',
      '#title' => $this->t('Secret Key'),
      '#description' => $this->t('The secret key given by PlaceToPay.'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];

    $form['test_endpoint_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('(Test) Endpoint URL'),
      '#description' => $this->t("The test PlaceToPay API endpoint URL for this environment. <strong>Don't add an / at the end</strong>, example https://thedomain.com"),
      '#default_value' => $this->configuration['test_endpoint_url'],
      '#required' => TRUE,
    ];

    $form['live_endpoint_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('(Live) Endpoint URL'),
      '#description' => $this->t("The live PlaceToPay API endpoint URL for this environment. <strong>Don't add an / at the end</strong>, example https://thedomain.com"),
      '#default_value' => $this->configuration['live_endpoint_url'],
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    if ($values['mode'] === 'live' && !$values['live_endpoint_url']) {
      $form_state->setError($form['live_endpoint_url'], $this->t('In Live mode must add the Live Endpoint URL.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['login_key'] = $values['login_key'];
    $this->configuration['secret_key'] = $values['secret_key'];
    $this->configuration['test_endpoint_url'] = $values['test_endpoint_url'];
    $this->configuration['live_endpoint_url'] = $values['live_endpoint_url'];
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $request_id = $order->getData('request_id');
    $response = $this->checkout_auth->getTransactionInfo($request_id);
    if (!$response || !isset($response->status->status)) {
      throw new PaymentGatewayException('error');
    }

    $p_status = $response->status->status;

    // Search for existing payment.
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $existing_payment = $payment_storage->loadByRemoteId($request_id);

    // If can't find the payment.
    if (!$existing_payment) {
      if ($p_status === 'APPROVED') {
        $current_time = \Drupal::time()->getCurrentTime();
        $payment = $payment_storage->create([
          'state' => 'completed',
          'amount' => $order->getTotalPrice(),
          'payment_gateway' => $this->parentEntity->id(),
          'order_id' => $order->id(),
          'remote_id' => $response->requestId,
          'remote_state' => $response->status->status,
          'authorized' => $current_time,
        ]);
        $payment->save();
        \Drupal::messenger()->addMessage($this->t('The payment was made successfully.'));
      }
      elseif ($p_status === 'OK' || $p_status === 'APPROVED_PARTIAL' || $p_status === 'PENDING' || $p_status === 'PENDING_VALIDATION' || $p_status === 'PENDING_PROCESS') {
        $payment = $payment_storage->create([
          'state' => 'authorization',
          'amount' => $order->getTotalPrice(),
          'payment_gateway' => $this->parentEntity->id(),
          'order_id' => $order->id(),
          'remote_id' => $response->requestId,
          'remote_state' => $p_status,
        ]);
        $payment->save();
        \Drupal::messenger()->addWarning($this->t('The payment was sent but is pending approval.'));
      }
      else {
        \Drupal::messenger()->addError($this->t('Error! Payment failed.'));
        throw new PaymentGatewayException('Error processing payment, please try again later');
      }
    }
    else {
      if ($p_status === 'APPROVED') {
        $existing_payment->setState('completed');
        $existing_payment->save();
      }
      elseif ($p_status === 'OK' || $p_status === 'APPROVED_PARTIAL' || $p_status === 'PENDING' || $p_status === 'PENDING_VALIDATION' || $p_status === 'PENDING_PROCESS') {
        $existing_payment->setState('authorization');
        $existing_payment->save();
      }
      else {
        \Drupal::messenger()->addError($this->t('Error! Payment failed.'));
        throw new PaymentGatewayException('Error processing payment, please try again later');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));

    // Get the transaction info.
    $request_id = $order->getData('request_id');
    $response = $this->checkout_auth->getTransactionInfo($request_id);
    if (!$response || !isset($response->status->status)) {
      throw new PaymentGatewayException('error');
    }

    $p_status = $response->status->status;

    // Redirect to error page
    $url = Url::fromRoute('commerce_placetopay.checkout_error', [
      'order' => $order->id(),
      'response_status' => strtolower($p_status),
    ]);
    $path = $url->toString();
    $response = new RedirectResponse($path);
    $response->send();
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    // Get the request data.
    $jsonParams = $request->getContent();
    $params = Json::decode($jsonParams);
    if (isset($params['requestId']) && isset($params['signature'])) {
      $requestId = $params['requestId'];
      $signature = $params['signature'];
      $statusObj = $params['status'];

      // Get payment object.
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->loadByRemoteId($requestId);

      // If can't find the payment.
      if (!$payment) {
        return FALSE;
      }

      // Validate Signature.
      $current_sha1 = sha1($requestId . $statusObj['status'] . $statusObj['date'] . $this->configuration['secret_key']);
      if ($current_sha1 === $signature) {
        // If everything is ok, then update the payment.
        if ($statusObj['status'] === 'APPROVED') {
          $payment->setState('completed');
          $payment->save();
        }
        if ($statusObj['status'] === 'REJECTED') {
          $payment->setState('authorization_voided');
          $payment->save();
        }
      }
      else {
        \Drupal::logger('commerce_placetopay')->warning('Func: onNotify(). Error validating signature. Request signature: @r_sign, Built signature: @b_sign, Request Id: @request_id.',
          ['@r_sign' => $signature, '@b_sign' => $current_sha1, '@request_id' => $requestId]
        );
        return FALSE;
      }
    }
    else {
      \Drupal::logger('commerce_placetopay')->warning('Func: onNotify(). Error updating the payment information.');
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutAuthorize(PaymentInterface $payment, array $extra) {
    $current_order = $payment->getOrder();
    $this->checkout_auth->authorize($payment, $extra);
    $current_order->setData('request_id', $this->checkout_auth->getRequestId());
    $current_order->setData('tranKey', $this->checkout_auth->getTranKey());
    $current_order->save();
    return $this->checkout_auth->getProcessUrl();
  }

}
