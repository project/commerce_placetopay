<?php

namespace Drupal\commerce_placetopay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Provides the interface for the Express Checkout payment gateway.
 */
interface RedirectCheckoutInterface {

  /**
   * DoVoid API Operation (NVP) request.
   *
   * Builds the data for the request and make the request.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param array $extra
   *   The extra data.
   *
   * @return string
   *   The process URL.
   *
   * @see https://developer.paypal.com/docs/classic/api/merchant/DoVoid_API_Operation_NVP/
   */
  public function checkoutAuthorize(PaymentInterface $payment, array $extra);

}
