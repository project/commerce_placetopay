<?php

namespace Drupal\commerce_placetopay\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements Redirect Checkout Payment Form.
 */
class RedirectCheckoutPaymentForm extends BasePaymentOffsiteForm {

  /**
   * The checkout auth object.
   *
   * @var \Drupal\commerce_placetopay\CheckoutAuth
   */
  protected $checkoutauth;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    if (!$this->hasPendingPayments()) {
      /** @var \Drupal\commerce_placetopay\Plugin\Commerce\PaymentGateway\RedirectCheckoutInterface $payment_gateway_plugin */
      $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

      $extra = [
        'return_url' => $form['#return_url'],
        'cancel_url' => $form['#cancel_url'],
      ];

      $redirect_url = $payment_gateway_plugin->checkoutAuthorize($payment, $extra);

      if (empty($redirect_url)) {
        throw new PaymentGatewayException(sprintf('Error processing the authetication.'));
      }

      return $this->buildRedirectForm($form, $form_state, $redirect_url, [], self::REDIRECT_GET);
    }
    else {
      // Get user id.
      $user_id = \Drupal::currentUser()->id();
      // Set error message.
      \Drupal::messenger()->addError(
        $this->t('Error! There are pending payments, can not perform new orders until the current payments are completed. <a href="/user/@uid/orders">See pending payments</a>', ['@uid' => $user_id])
      );
      throw new PaymentGatewayException(sprintf('Payment attempt when the user %s has pending payments.', \Drupal::currentUser()->id()));
    }
  }

  private function hasPendingPayments() {
    // Look for peding payments.
    $user_id = \Drupal::currentUser()->id();
    $query = \Drupal::database()->select('commerce_payment', 'cp');
    $query->addField('cp', 'payment_id');
    $query->join('commerce_order', 'co', 'cp.order_id = co.order_id');
    $query->condition('co.uid', $user_id);
    $query->condition('cp.state', 'authorization');
    $results = $query->execute()->fetchAll();

    if (!empty($results)) {
      return TRUE;
    }
    return FALSE;
  }

}
