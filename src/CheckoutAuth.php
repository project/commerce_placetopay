<?php

namespace Drupal\commerce_placetopay;

use Drupal\Component\Utility\Random;
use GuzzleHttp\Exception\RequestException;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Perform the main API functions.
 */
class CheckoutAuth implements CheckoutAuthInterface {
  use StringTranslationTrait;

  /**
   * Utility to generate random strings.
   *
   * @var \Drupal\Component\Utility\Random
   */
  protected $random;


  /**
   * The payment gateway plugin configuration.
   *
   * @var array
   */
  protected $config;

  /**
   * The process URL.
   *
   * @var string
   */
  protected $processURL;

  /**
   * The request Id.
   *
   * @var int
   */
  protected $requestId;

  /**
   * The tranKey generated per request.
   *
   * @var string
   */
  protected $tranKey;

  /**
   * The nonce generated per request.
   *
   * @var string
   */
  protected $nonce;

  /**
   * The seed generated per request.
   *
   * @var string
   */
  protected $seed;

  /**
   * Constructs a new CheckoutSdk object.
   */
  public function __construct(array $config) {
    $current_time = \Drupal::time()->getCurrentTime();
    $this->random = new Random();
    $this->config = $config;
    $this->seed = date('c', $current_time);
    $this->nonce = $this->random->string(15, TRUE);
    $this->tranKey = $this->generateTranKey();
  }

  /**
   * {@inheritdoc}
   */
  public function authorize(PaymentInterface $payment, array $extra) {
    $base_url = $this->getbaseurl();
    $nonceBase64 = base64_encode($this->nonce);
    $expiration = date('c', strtotime('15 minutes'));
    $login_key = $this->config['login_key'];
    $customer = $payment->getOrder()->getCustomer();
    $address = $payment->getOrder()->getBillingProfile()->get('address')->first()->getValue();

    if ($address) {
      $address_data = [
        'street' => $address['address_line1'] ?? '',
        'city' => $address['address_locality'] ?? '',
        'state' => $address['administrative_area'] ?? '',
        'postalCode' => $address['postal_code'] ?? '',
        'country' => $address['country_code'] ?? '',
      ];
    }

    if ($customer) {
      $document_id = isset($customer->get('field_document_id')->getValue()[0]['value']) ?
                     $customer->get('field_document_id')->getValue()[0]['value'] : '';
      $document_type = isset($customer->get('field_document_type')->getValue()[0]['value']) ?
                       $customer->get('field_document_type')->getValue()[0]['value'] : '';
      $phone = isset($customer->get('field_phone')->getValue()[0]['value']) ?
               $customer->get('field_phone')->getValue()[0]['value'] : '';

      $buyer_data = [
        'document' => $document_id,
        'documentType' => $document_type,
        'name' => $address['given_name'] ?? '',
        'surname' => $address['family_name'] ?? '',
        'email' => $customer->getEmail(),
        'mobile' => $phone,
        'address' => $address_data ?? [],
      ];
    }

    // Build the request body.
    $data = [
      'locale' => 'es_CR',
      'auth' => [
        'login' => $login_key,
        'tranKey' => $this->tranKey,
        'nonce' => $nonceBase64,
        'seed' => $this->seed,
      ],
      'payment' => [
        'reference' => $payment->getOrderId(),
        'description' => $this->getOrderDescription($payment->getOrder()),
        'amount' => [
          'currency' => $payment->getAmount()->getCurrencyCode(),
          'total' => $payment->getAmount()->getNumber(),
        ],
        'allowPartial' => FALSE,
      ],
      'buyer' => $buyer_data ?? [],
      'expiration' => $expiration,
      'returnUrl' => $extra['return_url'],
      'cancelUrl' => $extra['cancel_url'],
      'ipAddress' => \Drupal::request()->getClientIp(),
      'userAgent' => $_SERVER['HTTP_USER_AGENT'],
    ];

    // Initialize the http client.
    $client = \Drupal::httpClient();

    try {
      $responseJson = $client->post($base_url . '/api/session', [
        'timeout' => 10,
        'body' => json_encode($data, JSON_FORCE_OBJECT),
        'headers' => [
          'Content-Type' => 'application/json',
        ],
      ])->getBody()->getContents();
      $response = json_decode($responseJson);
      if (isset($response->processUrl) && isset($response->requestId)) {
        $this->processURL = $response->processUrl;
        $this->requestId = $response->requestId;
      }
      else {
        \Drupal::messenger()->addError($this->t('Error reading the response object.'));
      }
    }
    catch (RequestException $ex) {
      if ($ex->getResponse()) {
        $error = json_decode($ex->getResponse()->getBody()->getContents());
        if (isset($error->status->message)) {
          \Drupal::messenger()->addError($error->status->message);
        }
      }
      else {
        \Drupal::messenger()->addError($this->t('Authorization Error.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionInfo(string $request_id) {
    // Get necesary data.
    $base_url = $this->getbaseurl();
    $nonceBase64 = base64_encode($this->nonce);
    $login_key = $this->config['login_key'];

    // Build the request body.
    $data = [
      'auth' => [
        'login' => $login_key,
        'tranKey' => $this->tranKey,
        'nonce' => $nonceBase64,
        'seed' => $this->seed,
      ],
    ];

    // Initialize the http client.
    $client = \Drupal::httpClient();

    try {
      $responseJson = $client->post($base_url . '/api/session/' . $request_id, [
        'timeout' => 10,
        'body' => json_encode($data, JSON_FORCE_OBJECT),
        'headers' => [
          'Content-Type' => 'application/json',
        ],
      ])->getBody()->getContents();
      $response = json_decode($responseJson);
      if ($response) {
        return $response;
      }
      else {
        \Drupal::messenger()->addError($this->t('Error reading the response object.'));
        return NULL;
      }
    }
    catch (RequestException $ex) {
      $error = json_decode($ex->getResponse()->getBody()->getContents());
      if (isset($error->status->message)) {
        \Drupal::messenger()->addError($error->status->message);
      }
      return NULL;
    }
  }

  /**
   * Gets the API bass URL.
   *
   * @return string
   *   The base URL.
   */
  private function getbaseurl() {
    $mode = $this->config['mode'];
    if ($mode === 'live') {
      return $this->config['live_endpoint_url'];
    }
    else {
      return $this->config['test_endpoint_url'];
    }
  }

  /**
   * Generate a tranKey to be sent on request.
   *
   * @return string
   *   The tranKey generated.
   */
  private function generateTranKey() {
    if (isset($this->config['secret_key'])) {
      $secret_key = $this->config['secret_key'];
      $tranKey = base64_encode(sha1($this->nonce . $this->seed . $secret_key, TRUE));
      return $tranKey;
    }
    return NULL;
  }

  /**
   * Gets the given order description.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The current order.
   *
   * @return string
   *   The order description.
   */
  public function getOrderDescription(OrderInterface $order) {
    $description = '';
    // Get order items.
    $items = $order->getItems();
    foreach ($items as $item) {
      $title = $item->get('title')->getValue();
      $quantity = $item->get('quantity')->getValue();
      if (isset($title[0]['value'])) {
        $description .= "(" . round($quantity[0]['value'], 0) . ") ";
        $description .= $title[0]['value'] . ". ";
      }
    }
    return $description ? $description : $this->t('Not available.');
  }

  /**
   * {@inheritdoc}
   */
  public function getTranKey() {
    return $this->tranKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessUrl() {
    return $this->processURL;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestId() {
    return $this->requestId;
  }

}
