<?php

namespace Drupal\commerce_placetopay;

use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Implements Checkout Auth Interface.
 */
interface CheckoutAuthInterface {

  /**
   * Perform an authorize request.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The HTTP response.
   */
  public function authorize(PaymentInterface $payment, array $extra);

  /**
   * Perform an authorize request.
   *
   * @param string $request_id
   *   The HTTP response.
   *
   * @return object
   *   The process URL string.
   */
  public function getTransactionInfo(string $request_id);

  /**
   * Gets an process URL.
   *
   * @return string
   *   The process URL string.
   */
  public function getProcessUrl();

  /**
   * Gets the tranKey generated.
   *
   * @return string
   *   The tranKey generated.
   */
  public function getTranKey();

  /**
   * Gets the current request Id.
   *
   * @return int
   *   The request id.
   */
  public function getRequestId();

}
