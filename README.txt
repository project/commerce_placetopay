Commerce PlaceToPay Checkout

This module is under active development.
It will provide an integration with the PlaceToPay Web Checkout by Evertec.
More Information here: https://www.evertecinc.com/pasarela-de-pagos-e-commerce/

For Costa Rica:
This integration is processed by Banco de Costa Rica (BCR),
it needed to request the Web Checkout service to the BCR.
More information about the BCR Web Checkout:
https://www.bancobcr.com/wps/portal/bcr/bancobcr/comercios_afiliados/soluciones

Configuration:
1. Go to /admin/commerce/config/payment-gateways
2. Select the "PlaceToPay Checkout (Redirect to PlaceToPay Checkout)" Plugin.
3. Fill out all of the form information.

The notifications URL requested by PlaceToPay will be:
/payment/notify/[payment gateways id]

The "payment gateways id" can be seen here:
/admin/commerce/config/payment-gateways
