/**
 * @file
 * Condition UI behaviors.
 */

(function ($, window, Drupal) {

  'use strict';

  /**
   * Provide printing functionality.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the behavior for the condition summaries.
   */
  Drupal.behaviors.conditionSummary = {
    attach: function () {

      $('.print-page').off().on('click', function () {
        $('header').hide();
        $('footer').hide();
        $('.print-page').hide();
        window.print();
        $('header').show();
        $('footer').show();
        $('.print-page').show();
      });
    }
  };

})(jQuery, window, Drupal);
